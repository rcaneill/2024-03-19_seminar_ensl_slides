Compile:

```
latexmk -pdf slides.tex -pdflatex="pdflatex -interaction=nonstopmode"
```

compile with update when saving document:

```
latexmk -pdf slides.tex -pvc
```

## LICENCE

Licence is CC BY-SA for all the original content. Some figures have
a copyright or public domain licence (cf slides).
