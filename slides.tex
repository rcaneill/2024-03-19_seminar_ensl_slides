\documentclass[aspectratio=169]{beamer}

\input{includes}

\title[]{\textbf{\Large From \textcolor{redalpha}{alpha} to \textcolor{bluebeta}{beta} ocean}\\
  \small Exploring the role of surface buoyancy fluxes and seawater thermal expansion in setting the upper ocean stratification}
\author{Romain Caneill}

\begin{document}

\maketitlepage{}


\begin{frame}
  {Brief Curriculum \myhfill \url{romaincaneill.fr}}
  \begin{minipage}{3.5in}
    \begin{description}
      \item[2014 -- 2017] ENSL, physics and geophysics
      \item[2017 -- 2018] CAP de menuiserie
      \item[2018 -- 2024] PhD, Göteborg with Fabien Roquet
      \item[2024 -- 2027] Postdoc, Grenoble, SASIP project
    \end{description}

    \vspace{1cm}
    
    Some of my interests:
    \begin{multicols}{2}
      \begin{itemize}
      \item Python
      \item NEMO
      \item xnemogcm, xgcm
      \item Reproducible science
      \item Snakemake
      \item Free Software
      \end{itemize}
    \end{multicols}
    For the science, follow this presentation :)
  \end{minipage}
  \hfill
  \begin{minipage}{1.5in}
    \includegraphics[width=\textwidth]{picture_romain_caneill}\\
    \includegraphics[width=\textwidth]{contact}
  \end{minipage}
\end{frame}

\section{Introduction}



\begin{frame}
  {The oceans store carbon and heat}
  \begin{minipage}{3in}
    The oceans have taken up about:
    \begin{itemize}
    \item<1-> 25\,\% of CO$_2$ produced by human activities;
    \item<2> 90\,\% of excess heat.
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{2in}
    \onslide<2>{
      \includegraphics[width=\textwidth]{intro/ipcc_SRL-image-33a}
      }
  \end{minipage}

  \onslide<2>{
    \figadfromnote{ipcc2021chapter9}{the IPCC Sixth Report}
    }
\end{frame}


\begin{frame}{Ocean and atmosphere exchanges properties through the mixed layer}
  \begin{minipage}{.3\textwidth}
    \includegraphics[width=\textwidth]{intro/uop}
  \end{minipage}
  \hfill
  \begin{minipage}{.55\textwidth}
    \centering
    Winter MLD
    
  \includegraphics[width=\textwidth]{intro/mld_intro}
  \end{minipage}

  \figadfrom{sprintal_upper_2009}
  \hfill
  \figadfrom{johnson_gosml_2022}
\end{frame}


\begin{frame}{The ocean absorbs anthropogenic CO$_2$}
  \centgraph[width=4.5in]{intro/grubber_et_al_2023_fig1}

  \figadfrom{gruber_trends_2023}
\end{frame}


\begin{frame}{The global circulation brings the water properties at depth}
  \hfill
  \begin{minipage}[b]{3in}
    \centgraph{intro/global_circulation_meredith_2019}
  \end{minipage}
  \begin{minipage}[b]{1.5in}
  \figadfrom{meredith2019}
  \end{minipage}
\end{frame}


\begin{frame}{Ocean stratification}{WOCE A16 section of potential temperature}
  
  The large stratification inhibits vertical exchanges.

  \vfill
  
  \includegraphics[height=1.7in]{intro/A16_THETA_3}
  \hfill
  \includegraphics[height=1.7in]{intro/atlantic_click_map_edited}

  \vfill
  
  The ocean is mainly stratified because it is heated up at the surface.

  \vfill

  \tiny
  \hfill
  Figures adapted, \ccCopy{} 2011 International WOCE Office
\end{frame}


\begin{frame}<-4>{Regimes of stratification}
  \only<1>{\framesubtitle{T-S section IO9S \myhfill {\tiny https://cchdo.ucsd.edu/cruise/09AR20120105}}}
  \only<2>{\framesubtitle{Alpha ocean}}
  \only<3>{\framesubtitle{Beta ocean}}
  \only<4>{\framesubtitle{Transition zone}}

  % Stratification occurs because 1) temperature stratifies (decreases with depth)
  % or 2) salinity stratifies (increases with depth) or 3) both
  
  
  \begin{minipage}{4.2in}%{.7\textwidth}
    \only<1>{ \includegraphics[width=\textwidth]{intro/section}}
    \only<2>{ \includegraphics[width=\textwidth]{intro/section_alpha}}
    \only<4>{ \includegraphics[width=\textwidth]{intro/section_transition}}
    \only<3>{ \includegraphics[width=\textwidth]{intro/section_beta}}
    \only<5>{ \includegraphics[width=\textwidth]{intro/section_beta}}
  \end{minipage}
  \hfill
  \begin{minipage}{1.05in}%{.25\textwidth}
    \includegraphics[width=\textwidth]{intro/profiles_location}\\
    \only<2>{ \includegraphics[width=\textwidth]{intro/profile_alpha}}
    \only<4>{ \includegraphics[width=\textwidth]{intro/profile_transition}}
    \only<3>{ \includegraphics[width=\textwidth]{intro/profile_beta}}
    \only<5>{ \includegraphics[width=\textwidth]{intro/profile_beta}}
  \end{minipage}
\end{frame}


\begin{frame}{Beta, transition, and alpha}{T-S section IO9S, selected profiles}

  \centgraph[width=\textwidth]{intro/profiles}

  \begin{minipage}{.3\textwidth}
    \centering
    \onslide<2->
    \textcolor{bluebeta}{Salinity stratifies:\\
      beta ocean}
  \end{minipage}
  \hfill
  \begin{minipage}{.3\textwidth}
    \centering
    \onslide<3>
    Both stratify:\\
    transition zone
  \end{minipage}
  \hfill
  \begin{minipage}{.3\textwidth}
    \centering
    \onslide<1->
    \textcolor{redalpha}{Temperature stratifies:\\
      alpha ocean}
  \end{minipage}
  
  %% \begin{itemize}
  %% \item<1-> \textcolor{redalpha}{Temperature stratifies: alpha ocean}
  %% \item<2-> \textcolor{bluebeta}{Salinity stratifies: beta ocean}
  %% \item<3> Both stratify: transition zone
  %% \end{itemize}

  \vfill
  
  \tiny
  \hfill
  \citep{carmack_alpha/beta_2007}    
\end{frame}



\begin{frame}{Alpha and beta oceans}
  \begin{center}
  \includegraphics[width=4in]{intro/stewart_haine_map}
  \end{center}
  
  Called alpha -- beta oceans in reference to $\alpha$ and $\beta$, thermodynamic properties of seawater.
  
  \figadfrom{stewart_thermobaricity_2016}
\end{frame}




\begin{frame}{The thermal expansion coefficient (TEC, $\alpha$)}
  \begin{minipage}{3.8in}
    \begin{itemize}
    \item<1-> Cold water is usually denser than warm water.
    \item<2-> Ocean warms $\implies$ volume increases\\
      (1/2 of observed sea-level rise)
    \item<3-> The TEC quantifies the relative change of density with temperature:
      \[ \alpha = -\frac{1}{\rho} \pdv{\rho}{\Theta} \]
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{1.5in}
    \includegraphics[width=\textwidth]{intro/Thermal-Expansion}\\
    \tiny
    \hfill
    \ccPublicDomain{} Public Domain
  \end{minipage}  
\end{frame}


\begin{frame}{The haline contraction coefficient (HCC, $\beta$)}
  \begin{minipage}{3.8in}
    \begin{itemize}
    \item<1-> Salty water is denser than freshwater
    \item<3-> The HCC quantifies the relative change of density with salinity:
      \[ \beta = \frac{1}{\rho} \pdv{\rho}{S} \]
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{1.5in}
    
  \onslide<1->{
    \includegraphics[width=\textwidth]{intro/Egg-in-Salt-Water-Experiment}\\
    \tiny
    \hfill
    \ccCopy{} 2023 Science Sparks
  }

  \vspace*{.2in}
  
  \onslide<2->{
    \includegraphics[width=\textwidth]{intro/Floating_in_the_Dead_Sea}\\
    \tiny
    \ccAttribution{} aka4ajax
    }
  \end{minipage}  
\end{frame}


\begin{frame}{Properties of the TEC and HCC}
  \begin{minipage}{3.3in}
    \begin{itemize}
    \item<1-> The TEC follows a (quasi) linear relation with temperature
    \item<2-> The HCC variations in the ocean are negligible\\
      $\beta \simeq \SI{7.5e-4}{\kilo\gram\per\gram}$
    \item<3-> It was assumed that the role of salinity
      is enhanced in polar regions due to low values of the TEC
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{2in}
    \includegraphics[width=\textwidth]{paper_2_original/fig02}
  \end{minipage}

  \figadfrom{caneill_southern_2024}
  
\end{frame}




{
  \setbeamertemplate{footline}{}   
\begin{frame}{}%{Question}
  \centering
  \huge
  \vfill
  
  What is the origin of alpha and beta oceans?

  \vfill
\end{frame}
}

\setbeamercovered{transparent}
\begin{frame}{Objectives}
  \color{gray}
  \onslide<1,4>{\textcolor{mygreen}{From alpha to beta ocean:}}
  \onslide<2,3,4>{Exploring the role of}
  \onslide<2,4>{\textcolor{gublue}{surface buoyancy fluxes}}
  \onslide<4>{and}
  \onslide<3,4>{\textcolor{umber}{seawater thermal expansion}}
  \onslide<2,3,4>{in setting the \color{black} upper ocean stratification}

  \vfill

  \onslide<1,4>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{exampleblock}{Objective A}
        Describe alpha -- beta oceans using observations
        
        %% \hfill
        %% Paper III
      \end{exampleblock}
    \end{minipage}
  }
  \hfill
  \onslide<2,4>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{alertblock}{Objective B}
        How do buoyancy fluxes shape the upper stratification?
        %
        %\hfill
        %Papers I, II
      \end{alertblock}
    \end{minipage}
  }
  \hfill
  \onslide<3,4>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{block}{Objective C}
        Assess the role of the \mbox{local} value of the TEC.\\
        \mbox{~}
        %
        %\hfill
        %Papers I, II, and IV
      \end{block}
    \end{minipage}
  }

  \vfill

  % \tiny 
  % \begin{description}
  %     \item[\paperC]<2,3,4> \fullcite{caneill_polar_2022}
  %     \item[\paperCC]<2,3,4> \fullcite{caneill_southern_2024}
  %     \item[\paperCCC]<1,4> \fullcite{caneill_temperature}
  %     \item[\paperR]<3,4> \fullcite{roquet_unique_2022}
  % \end{description}

  \onslide<3,4>{\blfootnote{TEC = Thermal expansion coefficient}}
\end{frame}

%% \begin{frame}{This thesis}
%%   \begin{description}
%%       \item[\paperC] \fullcite{caneill_polar_2022}
%%       \item[\paperCC] \fullcite{caneill_southern_2024}
%%       \item[\paperCCC] \fullcite{caneill_temperature}
%%       \item[\paperR] \fullcite{roquet_unique_2022}
%%   \end{description}
%% \end{frame}

\begin{frame}{This presentation}{Paper \paperC}
  \centering
  \fullcite{caneill_polar_2022}

  \vfill
  
  \includegraphics[height=1.2in]{paper1_paper}\hfill
  \includegraphics[height=1.2in]{paper1_github}

  \vfill

  \hfill
  100\,\% reproducible with few commands

  \vfill
  
  
  \tiny
  \url{https://doi.org/10.1175/JPO-D-21-0295.1}
  \hfill
  \url{https://github.com/rcaneill/caneill-et-al-JPO-nemo-transition-zone}
\end{frame}

\begin{frame}{This presentation}{Paper \paperCC}
  \centering
  \fullcite{caneill_southern_2024}

  \vfill
  
  \includegraphics[height=1.2in]{paper2_preprint}\hfill
  \includegraphics[height=1.2in]{paper2_gitlab}

  \vfill


  \hfill
  100\,\% reproducible with few commands

  \vfill
  
  
  \tiny
  \url{https://doi.org/10.5194/egusphere-2023-2404}
  \hfill
  \url{https://gitlab.com/rcaneill/caneill-et-al-OS-SO-DMB}
\end{frame}


\begin{frame}{This presentation}{Paper \paperCCC}
  \centering
  \fullcite{caneill_temperature}
\end{frame}

\begin{frame}{This presentation}{Paper \paperR}
  \centering
  \fullcite{roquet_unique_2022}

  \vfill

  \includegraphics[height=1.2in]{paper4_paper}
\end{frame}


\section{A. Alpha -- beta}

\setbeamercolor{palette primary}{bg=lightyellow,fg=mygreen}
\setbeamercolor{palette secondary}{bg=lightyellow,fg=mygreen}
\setbeamercolor{palette tertiary}{bg=lightyellow,fg=mygreen}
\setbeamercolor{palette quaternary}{bg=lightyellow,fg=mygreen}
\setbeamercolor{enumerate item}{fg=mygreen}
\setbeamercolor{itemize item}{fg=mygreen}

\begin{frame}<1>{Objective A}
  \onslide<1>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{exampleblock}{Objective A}
        Describe alpha -- beta oceans using observations

        \hfill
        Paper III
      \end{exampleblock}
    \end{minipage}
  }
  \hfill
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{alertblock}{Objective B}
        How do buoyancy fluxes shape the upper stratification?

        \hfill
        Papers I, II
      \end{alertblock}
    \end{minipage}
  }
  \hfill
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{block}{Objective C}
        Assess the role of the \mbox{local} value of the TEC.\\
        \mbox{~}
        
        \hfill
        Papers I, II, and IV
      \end{block}
    \end{minipage}
  }
  \vfill

  %% Aim: Provide a general description of alpha – beta oceans based on
  %% observations.

  %% \vfill
  \footnotesize
  \begin{description}
    %\item[Paper \paperC] \fullcite{caneill_polar_2022}
    %\item[Paper \paperCC] \fullcite{caneill_southern_2024}
  \item[Paper \paperCCC] \fullcite{caneill_temperature}
    %\item[Paper \paperR] \fullcite{roquet_unique_2022}
  \end{description}
\end{frame}


% \begin{frame}{Describe alpha -- beta oceans \obj{A}}
%     Aim: Provide a general description of alpha – beta oceans based on
% observations.

% \vfill

% \begin{description}
%     %\item[Paper \paperC] \fullcite{caneill_polar_2022}
%     %\item[Paper \paperCC] \fullcite{caneill_southern_2024}
%     \item[Paper \paperCCC] \fullcite{caneill_temperature}
%     %\item[Paper \paperR] \fullcite{roquet_unique_2022}
% \end{description}
% \end{frame}

\begin{frame}{Stratification Control Index (SCI) \obj{A}}
  \begin{align}
    SCI = 
    \frac{\alpha \partial_z \Theta + \beta \partial_z S}
         {\alpha \partial_z \Theta - \beta \partial_z S} \label{eq:sci}
  \end{align}

  \vfill
  
  The SCI quantifies the relative effect of temperature and salinity on stratification.

  \vfill
  
  \begin{exampleblock}{}
    SCI > 1: alpha \hfill
    -1 < SCI < 1: transition \hfill
    SCI < -1: beta
  \end{exampleblock}
\end{frame}

\begin{frame}{Compute climatology of winter SCI}
  \begin{itemize}
  \item<1-> Based on about 20 years of observation profiles (EN4 database, includes ARGO, ship-based CTD, MEOP, etc)
  \item<2-> Compute the SCI at the bottom of winter mixed layer
  \item<3-> Interpolation to produce climatology
  \end{itemize}
\end{frame}


\begin{frame}[t]{Global maps of the winter SCI (Paper III) \obj{A}}
  \centering
  \begin{overprint}
  \only<1>{\includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/trop_climatology_global}}
  \only<2>{\includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/alpha_climatology_global}}
  \only<3>{\includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/ptz_climatology_global}}
  \only<4>{\includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/beta_climatology_global}}
  \end{overprint}
  
  \vfill
  
  \begin{minipage}{.5\textwidth}
    \begin{itemize}
    \item<1> Low-latitudes: transition zone
    \item<2> Mid-latitudes: alpha ocean
    \end{itemize}        
  \end{minipage}
  \hfill
  \begin{minipage}{.49\textwidth}
    \begin{itemize}
    \item<3> Between alpha and beta: PTZ
    \item<4> High-latitudes: beta ocean
    \end{itemize}        
  \end{minipage}
  \blfootnote{PTZ = polar transition zone}
\end{frame}

\begin{frame}[t]{Global maps of the winter SCI (Paper III) \obj{A}}{}
  \centering
  \includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/climatology_global}
  
  \vfill
  
  \begin{exampleblock}{}
    \begin{itemize}
    \item \textcolor{mygreen}{Zonation with: transition zone $\rightarrow$ alpha $\rightarrow$ PTZ $\rightarrow$ beta}
    \item \textcolor{mygreen}{Wide and zonal North Pacific PTZ}
    \item \textcolor{mygreen}{Narrow and diagonal North Atlantic PTZ}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}[t]{Global maps of the winter SCI (Paper III) \obj{A}}
  \centering
  \includegraphics[width=4.2in]{paper4/EN4.2.2_climatology_1_degree/sci_winter/with_mld_climatology_global}

  \vfill
  
  Deep MLs located at the poleward flank of alpha oceans.
\end{frame}

\begin{frame}{Relation with mixed layer depth (Paper III) \obj{A}}
  \begin{minipage}{.35\textwidth}
    \begin{exampleblock}{}
      \begin{itemize}
      \item<1-> \textcolor{mygreen}{Deep MLs mostly found in alpha oceans}
      \item<2> \textcolor{mygreen}{Bimodal distribution of the SCI, centred around $\pm 1.5$}
      \end{itemize}
    \end{exampleblock}
  \end{minipage}
  \hfill
  \begin{minipage}{.6\textwidth}
    \only<1>{\includegraphics[height=2.5in,trim={0 0 1.2in 1.2in},clip]{paper4/EN4.2.2_climatology_1_degree/sci_vs_mld/no_tropics_climatology_winter}}
    \only<2>{\includegraphics[height=2.5in,trim={0 0 0 1.2in},clip]{paper4/EN4.2.2_climatology_1_degree/sci_vs_mld/no_tropics_climatology_winter}}
  \end{minipage}
  \blfootnote{Figure for $\abs{\varphi} \geq \ang{30}$}
\end{frame}

\section{B. Buoyancy fluxes}

\setbeamercolor{palette primary}{bg=lightyellow,fg=gublue}
\setbeamercolor{palette secondary}{bg=lightyellow,fg=gublue}
\setbeamercolor{palette tertiary}{bg=lightyellow,fg=gublue}
\setbeamercolor{palette quaternary}{bg=lightyellow,fg=gublue}
\setbeamercolor{enumerate item}{fg=gublue}
\setbeamercolor{itemize item}{fg=gublue}


\begin{frame}<1>{Objective B}
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{exampleblock}{Objective A}
        Describe alpha -- beta oceans using observations

        \hfill
        Paper III
      \end{exampleblock}
    \end{minipage}
  }
  \hfill
  \onslide<1>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{alertblock}{Objective B}
        How do buoyancy fluxes shape the upper stratification?

        \hfill
        Papers I, II
      \end{alertblock}
    \end{minipage}
  }
  \hfill
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{block}{Objective C}
        Assess the role of the \mbox{local} value of the TEC.\\
        \mbox{~}
        
        \hfill
        Papers I, II, and IV
      \end{block}
    \end{minipage}
  }

  \vfill
  %% Aim: Investigate how the buoyancy fluxes shape the upper ocean stratification
  %% and contribute to the formation of alpha and beta oceans.


  %% \vfill
  \footnotesize
  
  \begin{description}
  \item[Paper \paperC] \fullcite{caneill_polar_2022}
  \item[Paper \paperCC] \fullcite{caneill_southern_2024}
    %\item[Paper \paperCCC] \fullcite{caneill_temperature}
    %\item[Paper \paperR] \fullcite{roquet_unique_2022}
  \end{description}
\end{frame}



% \begin{frame}{Buoyancy fluxes shape the stratification \obj{B}}
%    Aim: Investigate how the buoyancy fluxes shape the upper ocean stratification
%    and contribute to the formation of alpha and beta oceans.


%     \vfill

%     \begin{description}
%         \item[Paper \paperC] \fullcite{caneill_polar_2022}
%         \item[Paper \paperCC] \fullcite{caneill_southern_2024}
%         %\item[Paper \paperCCC] \fullcite{caneill_temperature}
%         %\item[Paper \paperR] \fullcite{roquet_unique_2022}
%     \end{description}
% \end{frame}

\begin{frame}{Numerical model (Paper I) \myhfill \includegraphics[height=.4cm]{paper1/nemo} \obj{B}}
  \begin{center}
    \includegraphics[trim={0 6cm 0 0},clip,width=4.5in]{paper1/bathy-forcing}
  \end{center}

  Idealised configuration that allows to study the role of annual buoyancy fluxes,
  by modification of the equation of state (thus changing the TEC).
\end{frame}

\begin{frame}{Annual buoyancy fluxes: competition (Paper I) \obj{B}}
  \begin{center}
    \includegraphics[width=4in]{paper1/fb-tot}
  \end{center}

  Reference run
\end{frame}

\begin{frame}{Annual buoyancy fluxes set the transition (Paper I) \obj{B}}
  \begin{center}
    \includegraphics[width=4in]{paper1/fb-comp}
  \end{center}

  Wind kept unchanged!
\end{frame}


\begin{frame}{Poleward shift of the PTZ with increased TEC (Paper I) \obj{B}}
  \begin{center}
    \includegraphics[width=4in]{paper1/mld-comp}
  \end{center}

  Will fronts move poleward due to increased ocean temperature?
\end{frame}

\begin{frame}{Poleward migration of transition zone due to global warming?}
  \includegraphics[width=.6\textwidth]{lique_thomas_2018_fig_3}
  \figfrom{lique_latitudinal_2018}  
\end{frame}


\begin{frame}{Annual buoyancy fluxes set the transition (Paper I) \obj{B}}
  \begin{center}
    \includegraphics[width=4in]{paper1/sketch}
  \end{center}
\end{frame}

%% \begin{frame}{Annual buoyancy fluxes set the transition (Paper I) \obj{B}}
%%   \begin{center}
%%     \includegraphics[width=3.5in]{paper1/sketch}
%%   \end{center}

%%   \begin{alertblock}{}
%%     \begin{itemize}
%%     \item<2> \textcolor{gublue}{Inversion of buoyancy fluxes above the PTZ}
%%     \item<2> \textcolor{gublue}{Positive annual buoyancy flux poleward of the deep MLs}
%%     \end{itemize}
%%   \end{alertblock}
%% \end{frame}

\begin{frame}{Winter buoyancy loss erodes stratification (Paper II) \obj{B}}
  \begin{minipage}{.45\textwidth}
    \includegraphics[width=\textwidth]{paper3/fig07}
  \end{minipage}
  \hfill
  \begin{minipage}{.5\textwidth}
    \begin{itemize}
    \item $B_{250}$: measure of stratification
    \item $\mathcal{B}^{CS}$: buoyancy loss
    \item Hatched region: the DMB
    \end{itemize}
    
    \vfill
    
    \begin{alertblock}{}
      \begin{itemize}
      \item<2> \textcolor{gublue}{The position of the deep MLs is set by the balance between buoyancy loss and stratification}
      \item<2> \textcolor{gublue}{Buoyancy fluxes control the stratification regimes}
      \end{itemize}
    \end{alertblock}
  \end{minipage}

  \blfootnote{DMB = deep mixing band}
\end{frame}


\begin{frame}{Winter buoyancy loss erodes stratification: zonal view (Paper II) \obj{B}}
    \includegraphics[width=\textwidth]{paper3/fig09a}
\end{frame}

\section{C. TEC}

\setbeamercolor{palette primary}{bg=lightyellow,fg=umber}
\setbeamercolor{palette secondary}{bg=lightyellow,fg=umber}
\setbeamercolor{palette tertiary}{bg=lightyellow,fg=umber}
\setbeamercolor{palette quaternary}{bg=lightyellow,fg=umber}
\setbeamercolor{enumerate item}{fg=umber}
\setbeamercolor{itemize item}{fg=umber}

\begin{frame}<1>{Objective C}
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{exampleblock}{Objective A}
        Describe alpha -- beta oceans using observations

        \hfill
        Paper III
      \end{exampleblock}
    \end{minipage}
  }
  \hfill
  \onslide<2>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{alertblock}{Objective B}
        How do buoyancy fluxes shape the upper stratification?

        \hfill
        Papers I, II
      \end{alertblock}
    \end{minipage}
  }
  \hfill
  \onslide<1>{
    \begin{minipage}[t]{.3\textwidth}
      \begin{block}{Objective C}
        Assess the role of the \mbox{local} value of the TEC.\\
        \mbox{~}
        
        \hfill
        Papers I, II, and IV
      \end{block}
    \end{minipage}
  }

  \vfill
  
  %% Aim : assess the role of the local value of the thermal expansion coefficient.

  %% \vfill

  \footnotesize
  \begin{description}
  \item[Paper \paperC] \fullcite{caneill_polar_2022}
  \item[Paper \paperCC] \fullcite{caneill_southern_2024}
    %\item[Paper \paperCCC] \fullcite{caneill_temperature}
  \item[Paper \paperR] \fullcite{roquet_unique_2022}
  \end{description}
\end{frame}

% \begin{frame}{Assess the role of the local value of the TEC \obj{C}}
%     Aim : assess the role of the local value of the TEC

%     \vfill

%     \begin{description}
%         \item[Paper \paperC] \fullcite{caneill_polar_2022}
%         \item[Paper \paperCC] \fullcite{caneill_southern_2024}
%         %\item[Paper \paperCCC] \fullcite{caneill_temperature}
%         \item[Paper \paperR] \fullcite{roquet_unique_2022}
%     \end{description}
% \end{frame}


\begin{frame}{The TEC varies with temperature (Paper IV) \obj{C}}

  \begin{itemize}
  \item Follows a (quasi) linear relation with temperature
  \item Decreases the impact of temperature and heat in polar regions
  \end{itemize}
  
  \begin{center}
    \includegraphics[width=\textwidth,trim={0 0 5cm 0},clip]{paper2/figure_2_tec}
  \end{center}
\end{frame}


\begin{frame}{Why does the TEC play a role?}
  \begin{center}
    The TEC scales the effect of 
  \end{center}
  \onslide<1->{
    temperature on stratification
    \begin{align}
      B_{250} &= \underbrace{
        \frac{g}{\Delta t} \int_{-250}^0 \pmb{\alpha}(z) \frac{\partial \Theta}{\partial z} z \mathrm{d}z
      }_{\textstyle
        \begin{gathered}
          B_{250}^\Theta
        \end{gathered}
      }
      \underbrace{
        - \frac{g}{\Delta t} \int_{-250}^0 \beta(z) \frac{\partial S}{\partial z} z \mathrm{d}z
      }_{\textstyle
        \begin{gathered}
          B_{250}^S
        \end{gathered}
      }
      \label{eq:b250_thermal_haline}
    \end{align}
  }

  \onslide<2->{
    heat fluxes on buoyancy fluxes
    \begin{align}
      \mathcal{B}^{surf} &= \underbrace{ \pmb{\alpha} \frac{g}{\rho_0 C_p} Q_{tot}}_{
        \textstyle
        \begin{gathered}
          \mathcal{B}_{\Theta}^{surf}
        \end{gathered}
      }
      \underbrace{\vphantom{\frac{g \alpha}{\rho_0 C_p} Q_{tot}}
        -\frac{g \beta S}{\rho_0} (E-P-R)}_{
        \textstyle
        \begin{gathered}
          \mathcal{B}_S^{surf}
        \end{gathered}
      } \label{eq:f_b}
    \end{align}
  }

  \blfootnote{$\pmb{\alpha}$ is the TEC}
  
\end{frame}

\begin{frame}{The impact of the variable TEC (Paper II) \obj{C}}
  \begin{minipage}{.43\textwidth}
    \begin{center}
      \includegraphics[width=\textwidth,trim={0 0 11cm 0},clip]{paper3/fig10}
    \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}{.55\textwidth}
    \begin{block}{}
      \onslide<2>{The decrease in the TEC:}
      \begin{itemize}
      \item<2> \textcolor{umber}{allows for stable beta ocean}
      \item<2> \textcolor{umber}{damps buoyancy loss in polar region}
      \end{itemize}
    \end{block}
  \end{minipage}
\end{frame}


\begin{frame}{The impact of the variable TEC (Paper II) \obj{C}}
  {$B_{250} - \mathcal{B}^{CS}$}
  \begin{minipage}{.35\textwidth}
    \onslide<1->{\includegraphics[width=\textwidth,trim={2.6cm 1cm 3cm 5.9cm},clip]{paper3/fig07}}
  \end{minipage}
  \hfill
  \begin{minipage}{.29\textwidth}
    \onslide<1->{$\longleftarrow$ variable TEC}
    
    \vspace*{.5in}

    \onslide+<2->{\hfill constant TEC $\alpha_0$ $\longrightarrow$}
  \end{minipage}
  \hfill
  \begin{minipage}{.35\textwidth}
  \onslide+<2->{\includegraphics[width=\textwidth,trim={2.6cm 1cm 3cm 5.9cm},clip]{paper3/fig11}}
  \end{minipage}
  %\hfill
  %\begin{minipage}{.55\textwidth}
    \begin{block}{}
      \begin{itemize}
      \item<3> \textcolor{umber}{The variable TEC controls the width of the DMB}
      \item<3> \textcolor{umber}{The decrease in the TEC limits the southward extent of the DMB}
      \item<3> \textcolor{umber}{Beta oceans exist because the TEC becomes small}
      \end{itemize}
    \end{block}
  %\end{minipage}

  %\blfootnote{DMB = deep mixing band, in the Southern Ocean}
\end{frame}

\begin{frame}
  {The polar value of the TEC as global controller (Papers II and IV)}
  \begin{minipage}{.45\textwidth}
    \includegraphics[width=\textwidth]{paper1/properties-vs-lat-c}
  \end{minipage}
  \hfill
  \begin{minipage}{.45\textwidth}
    \includegraphics[width=\textwidth]{paper2/roquet_SI}
  \end{minipage}
\end{frame}

%% \begin{frame}{Key results \obj{C}}
%%     \begin{itemize}
%%         \item In subtropics: large TEC $\implies$ large stratification 
%%     \end{itemize}
%% \end{frame}




\section{Conclusions}

\setbeamercolor{palette primary}{bg=lightyellow,fg=black}
\setbeamercolor{palette secondary}{bg=lightyellow,fg=black}
\setbeamercolor{palette tertiary}{bg=lightyellow,fg=black}
\setbeamercolor{palette quaternary}{bg=lightyellow,fg=black}
\setbeamercolor{enumerate item}{fg=gray}
\setbeamercolor{itemize item}{fg=gray}




%% \begin{frame}{Limitations and Future Work}

%%   \begin{itemize}
%%   \item Limited observations in the SO $\implies$ we focused on climatological state
%%   \item Numerical simulations use coarse resolution
%%   \item The equation of state will not change
%%   \item Hard to distinguish between the cabbeling effect and the local value of the TEC
%%   \item Egg or chicken? The link between alpha ocean and deep MLs
%%   \end{itemize}
%% \end{frame}



\begin{frame}
  {Conclusions}
  \onslide<1->{
    \begin{exampleblock}{Describe alpha -- beta oceans using observations. \hfill \obj{A}}
      \begin{itemize}
      \item \textcolor{mygreen}{Global zonation: alpha $\rightarrow$ transition zone $\rightarrow$ beta}
      \item \textcolor{mygreen}{ML deeper in alpha- than beta-oceans}
      %\item \textcolor{mygreen}{Large density compensation below the winter ML}
      \end{itemize}
    \end{exampleblock}
  }

  \onslide<2->{
    \begin{alertblock}{How do buoyancy fluxes shape the upper stratification? \hfill \obj{B}}
      \begin{itemize}
      \item \textcolor{gublue}{The transition zone is located at the sign inversion of annual buoyancy fluxes}
      \item \textcolor{gublue}{Buoyancy loss erodes stratification and produces the DMB}
      \end{itemize}
    \end{alertblock}
  }

  \onslide<3->{
    \begin{block}{Assess the role of the local value of the TEC. \hfill \obj{C}}
      \begin{itemize}
      \item \textcolor{umber}{The decrease in the TEC in polar regions decreases buoyancy loss}
      \item \textcolor{umber}{The small polar value of the TEC permits beta ocean formation}
      \item \textcolor{umber}{My thesis confirms that the origin of alpha -- beta oceans lies in thermodynamic of seawater}
      %\item \textcolor{umber}{Sea ice could not form if the TEC were larger}
      \end{itemize}
    \end{block}
  }
\end{frame}

\begin{frame}{Perspectives}
  \begin{itemize}
  \item The sea surface temperature exerts a strong control on the stratification by its link with TEC.
  \item Buoyancy fluxes are not simply the sum of heat and freshwater fluxes.
  \item Warming $\implies$ larger values of the TEC. But also increases freshwater fluxes in the polar regions. Who will win?
  \end{itemize}
\end{frame}

\appendix
\begin{frame}
  {References}
  \renewcommand*{\bibfont}{\fontsize{5pt}{6pt}\selectfont}
  \begin{multicols}{2}
    \printbibliography
  \end{multicols}
\end{frame}

\end{document}
